#!/bin/bash

pass thumbdrive-password |
    head -n 1 |
    udisksctl unlock -b /dev/sdc1
udisksctl mount -b /dev/dm-4 &&
rsync -rav /media/rsys/backup/ /home/rsys/ &&
sudo apt update -y &&
if [[ "$EUID" = 0 ]]; then
    echo "(1) already root"
else
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
        echo "(2) correct password"
    else
        echo "(3) wrong password"
        exit 1
    fi
fi
sudo apt install aptitude tasksel -y &&
sudo aptitude install xorg xmonad xinit lightdm haskell-platform cabal-install conky wicd wicd-curses vim vim-gtk mutt vifm compton sxiv feh zathura zsh unar suckless-tools libxss-dev make gcc fonts-hack libgcr-3-dev libwebkit2gtk-4.0-dev wget newsbeuter mpv gufw pavucontrol -y && 
cabal update &&
cabal install --flags="with_xft all_extensions" xmobar &&
cabal install xmonad-contrib --flags="-use_xft" &&
chsh -s $(which zsh)

