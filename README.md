color theme and dotfiles for tiling window manager <br />
xmonad and dmenu + vifm + conky <br />
<br />
![Screenshot](desktop.png)
INSTALLATION <br />
git clone https://gitlab.com/Rettig/xmonadetc.git <br />
<br />
copy the files into your $HOME fodler <br />
the conkybar is for 1600x900 displays configured<br/>
i prefer the "Hack" font you can install it with apt install fonts-hack , or sometimes apt install ttf-hack
<br />
the newSystemInstall.sh only works on deb/ubuntu
<br />
XMONAD install and compile<br />
<br />
apt install xmonad<br />
cd .xmonad/
<br />
xmonad --recompile
<br />

